import React from "react";
import SideNav from "../Sidenav";
import "./sideBar.css";
import Home from "../../containers/Home/Home";
import Teacher from "../../containers/Teachers";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { Col, Row } from "react-bootstrap";

const SideBar = () => {
  return (
    <Row className="d-flex flex-wrap">
      <Col>
        {[
          {
            value: "teachers",
            path: "/teachers",
          },
          {
            value: "attendance",
            path: "/teachers",
          },
          {
            value: "assignments",
            path: "/teachers",
          },
        ].map((val) => (
          <div>
            <SideNav name={val.value} path={val.path} />
          </div>
        ))}
      </Col>
      <Col>
        <div>hjgfdagf</div>
      </Col>
    </Row>
  );
};

export default SideBar;
